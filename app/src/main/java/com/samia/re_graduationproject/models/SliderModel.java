package com.samia.re_graduationproject.models;

public class SliderModel {
    private int imageSlider;
    private String TextTitle;
    private String TextDetails;

    public SliderModel(int imageSlider, String textTitle, String textDetails) {
        this.imageSlider = imageSlider;
        TextTitle = textTitle;
        TextDetails = textDetails;
    }

    public int getImageSlider() {
        return imageSlider;
    }

    public void setImageSlider(int imageSlider) {
        this.imageSlider = imageSlider;
    }

    public String getTextTitle() {
        return TextTitle;
    }

    public void setTextTitle(String textTitle) {
        TextTitle = textTitle;
    }

    public String getTextDetails() {
        return TextDetails;
    }

    public void setTextDetails(String textDetails) {
        TextDetails = textDetails;
    }
}
