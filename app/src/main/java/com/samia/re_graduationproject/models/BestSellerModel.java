package com.samia.re_graduationproject.models;

public class BestSellerModel {
    private int itemBestSellerModelImg;
    private String itemBestSellerModelTitle;
    private String itemBestSellerModelPrice;

    public BestSellerModel(int itemBestSellerModelImg, String itemBestSellerModelTitle, String itemBestSellerModelPrice) {
        this.itemBestSellerModelImg = itemBestSellerModelImg;
        this.itemBestSellerModelTitle = itemBestSellerModelTitle;
        this.itemBestSellerModelPrice = itemBestSellerModelPrice;
    }

    public int getItemBestSellerModelImg() {
        return itemBestSellerModelImg;
    }

    public void setItemBestSellerModelImg(int itemBestSellerModelImg) {
        this.itemBestSellerModelImg = itemBestSellerModelImg;
    }

    public String getItemBestSellerModelTitle() {
        return itemBestSellerModelTitle;
    }

    public void setItemBestSellerModelTitle(String itemBestSellerModelTitle) {
        this.itemBestSellerModelTitle = itemBestSellerModelTitle;
    }

    public String getItemBestSellerModelPrice() {
        return itemBestSellerModelPrice;
    }

    public void setItemBestSellerModelPrice(String itemBestSellerModelPrice) {
        this.itemBestSellerModelPrice = itemBestSellerModelPrice;
    }
}
