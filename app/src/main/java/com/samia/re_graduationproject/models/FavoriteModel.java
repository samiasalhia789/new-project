package com.samia.re_graduationproject.models;

public class FavoriteModel {
    private int itemFavoriteImg;
    private String itemFavoriteTitle;
    private String itemFavoritePrice;

    public FavoriteModel(int itemFavoriteImg, String itemFavoriteTitle, String itemFavoritePrice) {
        this.itemFavoriteImg = itemFavoriteImg;
        this.itemFavoriteTitle = itemFavoriteTitle;
        this.itemFavoritePrice = itemFavoritePrice;
    }

    public int getItemFavoriteImg() {
        return itemFavoriteImg;
    }

    public void setItemFavoriteImg(int itemFavoriteImg) {
        this.itemFavoriteImg = itemFavoriteImg;
    }

    public String getItemFavoriteTitle() {
        return itemFavoriteTitle;
    }

    public void setItemFavoriteTitle(String itemFavoriteTitle) {
        this.itemFavoriteTitle = itemFavoriteTitle;
    }

    public String getItemFavoritePrice() {
        return itemFavoritePrice;
    }

    public void setItemFavoritePrice(String itemFavoritePrice) {
        this.itemFavoritePrice = itemFavoritePrice;
    }
}
