package com.samia.re_graduationproject.models;

public class CategoryModel {

    private int itemCategoryImg;
    private String itemCategoryTv;

    public CategoryModel(int itemCategoryImg, String itemCategoryTv) {
        this.itemCategoryImg = itemCategoryImg;
        this.itemCategoryTv = itemCategoryTv;
    }

    public int getItemCategoryImg() {
        return itemCategoryImg;
    }

    public void setItemCategoryImg(int itemCategoryImg) {
        this.itemCategoryImg = itemCategoryImg;
    }

    public String getItemCategoryTv() {
        return itemCategoryTv;
    }

    public void setItemCategoryTv(String itemCategoryTv) {
        this.itemCategoryTv = itemCategoryTv;
    }
}