package com.samia.re_graduationproject.models;

public class FeaturedBooksModel {

    private int itemFeaturedBooksImg;
    private String itemFeaturedBooksTitle;
    private String itemFeaturedBooksPrice;

    public FeaturedBooksModel(int itemFeaturedBooksImg, String itemFeaturedBooksTitle, String itemFeaturedBooksPrice) {
        this.itemFeaturedBooksImg = itemFeaturedBooksImg;
        this.itemFeaturedBooksTitle = itemFeaturedBooksTitle;
        this.itemFeaturedBooksPrice = itemFeaturedBooksPrice;
    }

    public int getItemFeaturedBooksImg() {
        return itemFeaturedBooksImg;
    }

    public void setItemFeaturedBooksImg(int itemFeaturedBooksImg) {
        this.itemFeaturedBooksImg = itemFeaturedBooksImg;
    }

    public String getItemFeaturedBooksTitle() {
        return itemFeaturedBooksTitle;
    }

    public void setItemFeaturedBooksTitle(String itemFeaturedBooksTitle) {
        this.itemFeaturedBooksTitle = itemFeaturedBooksTitle;
    }

    public String getItemFeaturedBooksPrice() {
        return itemFeaturedBooksPrice;
    }

    public void setItemFeaturedBooksPrice(String itemFeaturedBooksPrice) {
        this.itemFeaturedBooksPrice = itemFeaturedBooksPrice;
    }
}
