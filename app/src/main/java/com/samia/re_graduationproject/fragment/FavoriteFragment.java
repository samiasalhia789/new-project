package com.samia.re_graduationproject.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.samia.re_graduationproject.R;
import com.samia.re_graduationproject.adapter.BestSellerAdapter;
import com.samia.re_graduationproject.adapter.FavoriteAdapter;
import com.samia.re_graduationproject.databinding.FragmentFavoriteBinding;
import com.samia.re_graduationproject.models.BestSellerModel;
import com.samia.re_graduationproject.models.FavoriteModel;

import java.util.ArrayList;


public class FavoriteFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private FragmentFavoriteBinding binding;

    public FavoriteFragment() {
    }

    public static FavoriteFragment newInstance(String param1, String param2) {
        FavoriteFragment fragment = new FavoriteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = binding.inflate(inflater, container, false);
        View view = binding.getRoot();

        ArrayList<FavoriteModel> favoriteModels = new ArrayList<>();
        favoriteModels.add(new FavoriteModel(R.drawable.category_image, "إينولا هولمز وقضية","$34.00"));
        favoriteModels.add(new FavoriteModel(R.drawable.categories_image, "إينولا هولمز وقضية","$34.00"));
        favoriteModels.add(new FavoriteModel(R.drawable.category_image, "إينولا هولمز وقضية","$34.00"));

        FavoriteAdapter favoriteAdapter = new FavoriteAdapter(favoriteModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,
                false);
        binding.recyclerViewFavorite.setLayoutManager(linearLayoutManager);
        binding.recyclerViewFavorite.setHasFixedSize(true);
        binding.recyclerViewFavorite.setAdapter(favoriteAdapter);
        return view;
    }
}