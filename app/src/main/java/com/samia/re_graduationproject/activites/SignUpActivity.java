package com.samia.re_graduationproject.activites;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.samia.re_graduationproject.databinding.ActivitySignUpBinding;

public class SignUpActivity extends AppCompatActivity {
    private ActivitySignUpBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignUpBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.participationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationData()) {
                    Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        }); //end setOnClickListener



        binding.signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationData()) {
                    Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        }); //end setOnClickListener
    }// endonCreate

    private static boolean isValidEmail(CharSequence tSequence) {
        return (!TextUtils.isEmpty(tSequence) && Patterns.EMAIL_ADDRESS.matcher(tSequence).matches());
    } // end isValidEmail

    private boolean validationData() {
        if (binding.nameEt.getText().toString().isEmpty()) {
            Toast.makeText(SignUpActivity.this, "يجب ادخال الاسم", Toast.LENGTH_SHORT).show();
            return false;
        } else if (binding.emailEt.getText().toString().isEmpty()) {
            Toast.makeText(SignUpActivity.this, "يجب ادخال الايميل", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!isValidEmail(binding.emailEt.getText().toString())) {
            Toast.makeText(SignUpActivity.this, "الايميل غير صالح", Toast.LENGTH_SHORT).show();

            return false;
        } else if (binding.passwordEt.getText().toString().isEmpty()) {
            Toast.makeText(SignUpActivity.this, "يجب ادخال كلمة المرور", Toast.LENGTH_SHORT).show();

            return false;
        }
        return true;
    } // end validationData()
}// end class