package com.samia.re_graduationproject.activites;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.samia.re_graduationproject.R;
import com.samia.re_graduationproject.adapter.BestSellerAdapter;
import com.samia.re_graduationproject.adapter.CategoryAdapter;
import com.samia.re_graduationproject.adapter.FeaturedBooksAdapter;
import com.samia.re_graduationproject.databinding.ActivityHomeBinding;
import com.samia.re_graduationproject.models.BestSellerModel;
import com.samia.re_graduationproject.models.CategoryModel;
import com.samia.re_graduationproject.models.FeaturedBooksModel;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {
    private ActivityHomeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        ArrayList<CategoryModel> list = new ArrayList<>();
        list.add(new CategoryModel(R.drawable.categories_image, "ابطال المستقبل"));
        list.add(new CategoryModel(R.drawable.category_image, "علم النفس "));
        list.add(new CategoryModel(R.drawable.categories_image, "التنمية البشرية" +
                " و تطوير الذات"));
        list.add(new CategoryModel(R.drawable.category_image, "تاريخ"));


        CategoryAdapter adapter = new CategoryAdapter(list);
        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,
                false);
        binding.recyclerView.setLayoutManager(manager);
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setAdapter(adapter);



        ArrayList<FeaturedBooksModel> booksModelArrayList = new ArrayList<>();
        booksModelArrayList.add(new FeaturedBooksModel(R.drawable.category_image, "إينولا هولمز وقضية","$34.00"));
        booksModelArrayList.add(new FeaturedBooksModel(R.drawable.categories_image, "إينولا هولمز وقضية","$34.00"));
        booksModelArrayList.add(new FeaturedBooksModel(R.drawable.category_image, "إينولا هولمز وقضية","$34.00"));

        FeaturedBooksAdapter featuredBooksAdapter = new FeaturedBooksAdapter(booksModelArrayList);
        LinearLayoutManager layoutManagerer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,
                false);
        binding.recyclerViewFeaturedBooks.setLayoutManager(layoutManagerer);
        binding.recyclerViewFeaturedBooks.setHasFixedSize(true);
        binding.recyclerViewFeaturedBooks.setAdapter(featuredBooksAdapter);




        ArrayList<BestSellerModel> bestSellerModels = new ArrayList<>();
        bestSellerModels.add(new BestSellerModel(R.drawable.category_image, "إينولا هولمز وقضية","$34.00"));
        bestSellerModels.add(new BestSellerModel(R.drawable.categories_image, "إينولا هولمز وقضية","$34.00"));
        bestSellerModels.add(new BestSellerModel(R.drawable.category_image, "إينولا هولمز وقضية","$34.00"));

        BestSellerAdapter bestSellerAdapter = new BestSellerAdapter(bestSellerModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,
                false);
        binding.recyclerViewBestSeller.setLayoutManager(linearLayoutManager);
        binding.recyclerViewBestSeller.setHasFixedSize(true);
        binding.recyclerViewBestSeller.setAdapter(bestSellerAdapter);
    }
}