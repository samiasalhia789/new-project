package com.samia.re_graduationproject.activites;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.samia.re_graduationproject.databinding.ActivityForgetPasswordBinding;

public class ForgetPasswordActivity extends AppCompatActivity {
    private ActivityForgetPasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityForgetPasswordBinding.inflate(getLayoutInflater());

        View view = binding.getRoot();
        setContentView(view);

        binding.passwordRecoveryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationData()) {
                    Intent intent = new Intent(ForgetPasswordActivity.this, VerificationCodeActivity.class);
                    startActivity(intent);
                }
            }
        });// endsetOnClickListener


        binding.signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Intent intent = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
                    startActivity(intent);

            }
        });// endsetOnClickListener

    }
    private static boolean isValidEmail(CharSequence tSequence) {
        return (!TextUtils.isEmpty(tSequence) && Patterns.EMAIL_ADDRESS.matcher(tSequence).matches());
    } // end isValidEmail

    private boolean validationData() {
        if (binding.emailEt.getText().toString().isEmpty()) {
            Toast.makeText(ForgetPasswordActivity.this, "يجب ادخال الايميل", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!isValidEmail(binding.emailEt.getText().toString())) {
            Toast.makeText(ForgetPasswordActivity.this, "الايميل غير صالح", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    } // end validationData()
} // end class