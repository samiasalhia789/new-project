package com.samia.re_graduationproject.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.samia.re_graduationproject.R;
import com.samia.re_graduationproject.adapter.IntroViewPagerAdapter;
import com.samia.re_graduationproject.databinding.ActivitySliderActivityBinding;
import com.samia.re_graduationproject.models.SliderModel;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import java.util.ArrayList;
import java.util.List;

public class SliderActivity extends AppCompatActivity {
    private ActivitySliderActivityBinding binding;
    private ViewPager screenPager;
    IntroViewPagerAdapter introViewPagerAdapter;
    TabLayout tabIndicator;
    int position = 0;
    Button sliderActivityNextBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySliderActivityBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        tabIndicator = binding.tabIndicator;
        sliderActivityNextBtn = binding.nextBtn;


        final List<SliderModel> mList = new ArrayList<>();
        mList.add(new SliderModel(R.drawable.ic_on_boarding, "هدفنا مرضاتكم", "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص "));
        mList.add(new SliderModel(R.drawable.ic_on_boarding2, "نسهل عليكم اختياركم", "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص "));
        mList.add(new SliderModel(R.drawable.ic_on_boarding3, "نجعل تسوّقك أسهل", "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص "));


        // setup viewpager
        screenPager = binding.viewPager;
        introViewPagerAdapter = new IntroViewPagerAdapter(this, mList);
        screenPager.setAdapter(introViewPagerAdapter);

        // setup tablayout with viewpager
        tabIndicator.setupWithViewPager(screenPager);

        sliderActivityNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = screenPager.getCurrentItem();
                if (position < mList.size()) {
                    position++;
                    screenPager.setCurrentItem(position);
                }

                if (position == mList.size() - 1) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(SliderActivity.this, WelcomeActivity.class);
                            startActivity(intent);
                        }
                    }, 1000);
                }
            }
        });//end setOnClickListener

    } // end onCreate
} // end class