package com.samia.re_graduationproject.activites;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.samia.re_graduationproject.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationData()) {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }


            }
        }); // end setOnClickListener

        binding.forgotPasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);

            }
        });// endsetOnClickListener


        binding.createAnAccountTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);

            }
        });//end setOnClickListener

    } //end onCreate

    private static boolean isValidEmail(CharSequence tSequence) {
        return (!TextUtils.isEmpty(tSequence) && Patterns.EMAIL_ADDRESS.matcher(tSequence).matches());
    } // end isValidEmail

    private boolean validationData() {
        if (binding.emailEt.getText().toString().isEmpty()) {
            Toast.makeText(LoginActivity.this, "يجب ادخال الايميل", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!isValidEmail(binding.emailEt.getText().toString())) {
            Toast.makeText(LoginActivity.this, "الايميل غير صالح", Toast.LENGTH_SHORT).show();

            return false;
        } else if (binding.passwordEt.getText().toString().isEmpty()) {
            Toast.makeText(LoginActivity.this, "يجب ادخال كلمة المرور", Toast.LENGTH_SHORT).show();

            return false;
        }
        return true;
    } // end validationData()

}// end class