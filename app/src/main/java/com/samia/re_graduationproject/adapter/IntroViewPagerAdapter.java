package com.samia.re_graduationproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.samia.re_graduationproject.R;
import com.samia.re_graduationproject.models.SliderModel;

import java.util.List;

public class IntroViewPagerAdapter extends PagerAdapter {

   Context mContext ;
   List<SliderModel> mListScreen;

    public IntroViewPagerAdapter(Context mContext, List<SliderModel> mListScreen) {
        this.mContext = mContext;
        this.mListScreen = mListScreen;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutScreen = inflater.inflate(R.layout.item_slider,null);

        ImageView imgSlide = layoutScreen.findViewById(R.id.image);
        TextView title = layoutScreen.findViewById(R.id.titleTv);
        TextView Details = layoutScreen.findViewById(R.id.detailsTv);

        imgSlide.setImageResource(mListScreen.get(position).getImageSlider());
        title.setText(mListScreen.get(position).getTextTitle());
        Details.setText(mListScreen.get(position).getTextDetails());
        container.addView(layoutScreen);
        return layoutScreen;
    }

    @Override
    public int getCount() {
        return mListScreen.size();
    }
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
