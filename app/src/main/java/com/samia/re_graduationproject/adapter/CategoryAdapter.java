package com.samia.re_graduationproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.samia.re_graduationproject.R;
import com.samia.re_graduationproject.models.CategoryModel;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyHolder> {

    ArrayList<CategoryModel> category;
    Context context;

    public CategoryAdapter(ArrayList<CategoryModel> items) {
        this.category = items;
    }


    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_categories, null, false);
        MyHolder holder = new MyHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        CategoryModel Item = category.get(position);
        holder.itemCategoryTv.setText(Item.getItemCategoryTv());
        holder.itemCategoryImg.setImageResource(Item.getItemCategoryImg());


    }

    @Override
    public int getItemCount() {
        return category.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        ImageView itemCategoryImg;
        TextView itemCategoryTv;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            itemCategoryImg = itemView.findViewById(R.id.categoryImg);
            itemCategoryTv = itemView.findViewById(R.id.categoryTv);
        }
    }

}
