package com.samia.re_graduationproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.samia.re_graduationproject.R;
import com.samia.re_graduationproject.models.FavoriteModel;


import java.util.ArrayList;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.MyHolder> {

    ArrayList<FavoriteModel> favoriteModels;
    Context context;

    public FavoriteAdapter(ArrayList<FavoriteModel> items) {
        this.favoriteModels = items;
    }


    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorite, null, false);
        MyHolder holder = new MyHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        FavoriteModel Item = favoriteModels.get(position);
        holder.itemFavoriteTitle.setText(Item.getItemFavoriteTitle());
        holder.itemFavoritePrice.setText(Item.getItemFavoritePrice());
        holder.itembFavoriteImg.setImageResource(Item.getItemFavoriteImg());


    }

    @Override
    public int getItemCount() {
        return favoriteModels.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        ImageView itembFavoriteImg;
        TextView itemFavoriteTitle;
        TextView itemFavoritePrice;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            itembFavoriteImg = itemView.findViewById(R.id.featuredBooksImg);
            itemFavoriteTitle = itemView.findViewById(R.id.featuredBooksTitle);
            itemFavoritePrice = itemView.findViewById(R.id.featuredBooksPrice);
        }
    }

}
