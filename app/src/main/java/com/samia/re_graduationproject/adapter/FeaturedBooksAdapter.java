package com.samia.re_graduationproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.samia.re_graduationproject.R;
import com.samia.re_graduationproject.models.FeaturedBooksModel;

import java.util.ArrayList;

public class FeaturedBooksAdapter extends RecyclerView.Adapter<FeaturedBooksAdapter.MyHolder> {

    ArrayList<FeaturedBooksModel> featuredBooksModels;
    Context context;

    public FeaturedBooksAdapter(ArrayList<FeaturedBooksModel> items) {
        this.featuredBooksModels = items;
    }


    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_featured_books, null, false);
        MyHolder holder = new MyHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        FeaturedBooksModel Item = featuredBooksModels.get(position);
        holder.itemfeaturedBooksTitle.setText(Item.getItemFeaturedBooksTitle());
        holder.itemfeaturedBooksPrice.setText(Item.getItemFeaturedBooksPrice());
        holder.itemfeaturedBooksImg.setImageResource(Item.getItemFeaturedBooksImg());


    }

    @Override
    public int getItemCount() {
        return featuredBooksModels.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        ImageView itemfeaturedBooksImg;
        TextView itemfeaturedBooksTitle;
        TextView itemfeaturedBooksPrice;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            itemfeaturedBooksImg = itemView.findViewById(R.id.featuredBooksImg);
            itemfeaturedBooksTitle = itemView.findViewById(R.id.featuredBooksTitle);
            itemfeaturedBooksPrice = itemView.findViewById(R.id.featuredBooksPrice);
        }
    }

}
