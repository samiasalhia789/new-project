package com.samia.re_graduationproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.samia.re_graduationproject.R;
import com.samia.re_graduationproject.models.BestSellerModel;

import java.util.ArrayList;

public class BestSellerAdapter extends RecyclerView.Adapter<BestSellerAdapter.MyHolder> {

    ArrayList<BestSellerModel> bestSellerModels;
    Context context;

    public BestSellerAdapter(ArrayList<BestSellerModel> items) {
        this.bestSellerModels = items;
    }


    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_best_seller, null, false);
        MyHolder holder = new MyHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        BestSellerModel Item = bestSellerModels.get(position);
        holder.itemBestSelleTitle.setText(Item.getItemBestSellerModelTitle());
        holder.itemBestSellePrice.setText(Item.getItemBestSellerModelPrice());
        holder.itembBestSellerImg.setImageResource(Item.getItemBestSellerModelImg());


    }

    @Override
    public int getItemCount() {
        return bestSellerModels.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        ImageView itembBestSellerImg;
        TextView itemBestSelleTitle;
        TextView itemBestSellePrice;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            itembBestSellerImg = itemView.findViewById(R.id.bestSellerImg);
            itemBestSelleTitle = itemView.findViewById(R.id.bestSellerTitle);
            itemBestSellePrice = itemView.findViewById(R.id.bestSellerPrice);
        }
    }

}
